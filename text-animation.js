var timer;

const TypeText = (text,speed = 700, id) => {
    clearTimeout(timer);
    document.getElementById(id).innerHTML = '';
    var txt = text;
    var i = 0;
    const typing = () => {
        if (i < txt.length) {
            document.getElementById(id).innerHTML += txt.charAt(i);
            i++;
            timer = setTimeout(typing, speed);
        }
    }
    typing();
}

TypeText('World Leading Brands',500, 'write_text');
