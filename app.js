window.addEventListener("DOMContentLoaded", function () {
  // animation for the who worked with section
  // let proxy = { skew: 0 },
  //   skewSetter = gsap.quickSetter(".skewElem", "skewY", "deg"), // fast
  //   clamp = gsap.utils.clamp(-20, 20); // don't let the skew go beyond 20 degrees.

  // ScrollTrigger.create({
  //   onUpdate: (self) => {
  //     let skew = clamp(self.getVelocity() / -300);
  //     // only do something if the skew is MORE severe. Remember, we're always tweening back to 0, so if the user slows their scrolling quickly, it's more natural to just let the tween handle that smoothly rather than jumping to the smaller skew.
  //     if (Math.abs(skew) > Math.abs(proxy.skew)) {
  //       proxy.skew = skew;
  //       gsap.to(proxy, {
  //         skew: 0,
  //         duration: 1,
  //         ease: "power3",
  //         overwrite: true,
  //         onUpdate: () => skewSetter(proxy.skew),
  //       });
  //     }
  //   },
  // });

  // make the right edge "stick" to the scroll bar. force3D: true improves performance
  // gsap.set(".skewElem", { transformOrigin: "right center", force3D: true });

  // scroll animation for the who worked with section

  // gsap.to(".skewElem", {
  //   y: 0,
  //   x: 0,
  //   duration: 2,
  //   opacity: 10,
  //   ease: "power.out",
  //   scrollTrigger: {
  //     trigger: ".whoWorkWithAnimationStart",
  //     start: "top center",
  //     end: 'bottom top',
  //     // end : "bottom top",
  //     toggleActions: "play none none none",
  //     id: "::who worked with",
  //     markers: {
  //       startColor: "green",
  //       endColor: "red",
  //       fontWeight: "bold",
  //     },
  //   },
  // });

  // let tween = gsap.to(".companyWork", {
  //   duration: 1,
  //   rotation: 360,
  //   opacity: 1,
  //   delay: 1,
  //   stagger: 0.2,
  //   ease: "sine.out",
  //   force3D: true,
  //   id: ":company work",
  //   scrollTrigger: ".companyWorkingLogoSection",
  // });

  // let st = ScrollTrigger.create({
  //   trigger: ".companyWorkingLogoSection",
  //   markers: {
  //     startColor: "green",
  //     endColor: "red",
  //     fontWeight: "bold",
  //   },
  //   start: "center center",
  //   end: "+=700",
  //   animation: tween,
  //   toggleActions: "play none none reverse",
  // });

  // st.play();

  // gsap.to(".companyWork", {
  //   duration: 1,
  //   rotation: 360,
  //   opacity: 1,
  //   delay: 1,
  //   stagger: 0.2,
  //   ease: "sine.out",
  //   force3D: true,
  //   scrollTrigger: {
  //     trigger: ".companyWorkingLogoSection",
  //     start: "center center",
  //     toggleActions: "play none none none",
  //     id: "::company work",
  //     markers: {
  //       startColor: "green",
  //       endColor: "red",
  //       fontWeight: "bold",
  //     },
  //   },
  // });

  gsap.to(".companyWork", {
    duration: 2,
    rotation: 360,
    opacity: 1,
    delay: 0.5,
    stagger: 0.2,
    ease: "sine.out",
    force3D: true,
    scrollTrigger: {
      trigger: ".skewElem",
      start: "top center",
      toggleActions: "play none none pause",
      id: "::company",
      // markers: {
      //   startColor: "green",
      //   endColor: "red",
      //   fontWeight: "bold",
      // },
    },
  });

  // animation for cost effeciency
  gsap.set(".our-eff-heading", { x: 200, y: 0, opacity: 0 });
  gsap.to(".our-eff-heading", {
    y: 0,
    x: 0,
    duration: 2,
    opacity: 10,
    ease: "power.out",
    scrollTrigger: {
      trigger: ".our-eff-heading",
      start: "top center",
      toggleActions: "play none none none",
      id: "cost efficiency",
      // markers: {
      //   startColor: "green",
      //   endColor: "red",
      //   fontWeight: "bold",
      // },
    },
  });

  gsap.set(".cost-eff-animation", { x: -50, y: 0, opacity: 0 });
  gsap.to(".cost-eff-animation", {
    y: 0,
    x: 0,
    duration: 1,
    opacity: 10,
    ease: "power.out",
    scrollTrigger: {
      trigger: ".cost-eff-animation",
      start: "top center",
      toggleActions: "play none none reverse",
      id: "cost efficiency",
      // markers: {
      //   startColor: "green",
      //   endColor: "red",
      //   fontWeight: "bold",
      // },
    },
  });

  // animation for hero sections
  gsap.set(".animation-1", { x: 0, y: 0, visibility: "visible" });
  gsap.set(".animation-2", { x: 0, y: 20, opacity: 0, visibility: "visible" });
  gsap.set(".animation-3", { x: 0, y: 0, opacity: 0, visibility: "visible" });

  const anim1 = document.getElementById("anim1");
  const anim2 = document.getElementById("anim2");
  // const anim3 = document.getElementById("anim3");

  anim1.addEventListener("click", () => {
    gsap.to(".animation-1", {
      y: -40,
      opacity: 0,
      duration: 1,
    });

    gsap.to(".animation-2", {
      y: -40,
      duration: 1,
      opacity: 1,
      ease: "cir.out",
      delay: 1,
    });
    gsap.set(".anim-img", { rotation: -10, x: -10, opacity: 0, delay: 1 });
    gsap.to(".anim-img", {
      rotation: 0,
      x: 0,
      duration: 1,
      opacity: 10,
      ease: "cir.out",
      delay: 1,
    });
  });

  anim2.addEventListener("click", () => {
    gsap.set(".animation-1", { x: 0, y: 0, opacity: 0 });

    gsap.to(".animation-2", {
      y: -60,
      opacity: 0,
      duration: 1,
    });
    gsap.to(".animation-3", {
      y: -70,
      duration: 1,
      opacity: 1,
      delay: 1,
      ease: "cir.out",
    });

    gsap.set(".anim-img", { rotation: -10, x: -10, opacity: 0, delay: 1 });
    gsap.to(".anim-img", {
      rotation: 0,
      x: 0,
      duration: 1,
      opacity: 10,
      ease: "cir.out",
      delay: 1,
    });
  });

  // const btnAnimation = document.getElementById("btn-animation");
  // btnAnimation.addEventListener("mouseenter", () => {
  //   console.log("mouse enter");
  //   gsap.to(".btn-animation", {
  //     width: "150px",
  //     duration: 1,
  //     ease: "power.out",
  //   });
  //   gsap.to(".details-show", {
  //     width: "auto",
  //     duration: 1,
  //     ease: "power.out",
  //     display: "block"
  //   });

  //   // const viewDetails = document.getElementById("details-show")
  //   // viewDetails.style.display = "block";
  // });
  // btnAnimation.addEventListener("mouseleave", () => {
  //   console.log("mouse enter");

  //   gsap.to(".btn-animation", {
  //     width: "auto",
  //     duration: 1,
  //     ease: "power.out",
  //   });

  //   gsap.to(".details-show", {
  //     width: "auto",
  //     duration: 1,
  //     ease: "power.out",
  //     display: "none"
  //   });

  //   // const viewDetails = document.getElementById("details-show")
  //   // viewDetails.style.display = "none";
  // });

  // hire us section
  
  document.getElementById("tab-contents").classList.add("scale-100");
  
  let tabsContainer = document.querySelector("#tabs");
  let tabTogglers = tabsContainer.querySelectorAll("#tabs a");
  tabTogglers.forEach(function (toggler) {
    toggler.addEventListener("click", function (e) {
      e.preventDefault();

      document.getElementById('tab-contents').classList.remove('scale-100');
      setTimeout(()=> {
        document.getElementById('tab-contents').classList.add('scale-100');
      },500)


      let tabName = this.getAttribute("href");
      let tabContents = document.querySelector("#tab-contents");
      for (let i = 0; i < tabContents.children.length; i++) {
        tabTogglers[i].parentElement.classList.remove(
          "border-t",
          "border-r",
          "border-l",
          "-mb-px",
          "bg-white"
        );
        tabContents.children[i].classList.remove("hidden");
        if ("#" + tabContents.children[i].id === tabName) {
          continue;
        }
        tabContents.children[i].classList.add("hidden");
      }
      e.target.parentElement.classList.add(
        "border-t",
        "border-r",
        "border-l",
        "-mb-px",
        "bg-white"
      );
    });
  });
});
